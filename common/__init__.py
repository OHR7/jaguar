POSITIONS = ['blue1', 'blue2', 'blue3', 'blue4', 'blue5', 'red1', 'red2', 'red3', 'red4', 'red5']
SEASONS = ['Opening_Season', 'Closing_Season']
LEAGUE_SLUGS = ['LLN', 'LLA']
YEARS = ['2017_Season', '2018_Season', '2019_Season']


BASE_LEAGUE_URL = 'https://api.lan.lolesports.com/v2/schedules?tournamentId=63&tournamentTitle=lla_2019_apertura'
BASE_GAME_URL = 'https://acs.leagueoflegends.com/v1/stats/game/ESPORTSTMNT0{}/{}?gameHash={}'

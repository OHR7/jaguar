import requests

from common import BASE_LEAGUE_URL, BASE_GAME_URL


class Games:
    def __init__(self):
        pass

    @staticmethod
    def get_games_ids():
        r = requests.get(BASE_LEAGUE_URL)
        r_json = r.json()
        games_ids = []
        for game in r_json['schedules']:
            game_info = {}
            if game['gamesInfo'][0]['closed']:  # Checks if the game was already played
                game_id = game['gamesInfo'][0]['gameId']
                game_url = game['gamesInfo'][0]['matchHistory']
                game_info['game_id'] = game_id
                game_info['team_1'] = game['participant1']['id']
                game_info['team_2'] = game['participant2']['id']
                params = game_url.split('?')
                for p in params:
                    if 'gameHash' in p:
                        param = p.split('=')
                        game_hash = param[1]
                        game_info['game_hash'] = game_hash
                games_ids.append(game_info)
        return games_ids

    @staticmethod
    def get_game_overview(game_id, game_hash, team_1, team_2):
        for i in range(1, 4):
            try:
                game_url = BASE_GAME_URL.format(i, game_id, game_hash)
                r = requests.get(game_url)
                game_json = r.json()
                break
            except Exception:
                pass
        game = {
            'duration': game_json['gameDuration'],
            'id': game_json['gameId'],
            'patch': game_json['gameVersion'],
            'season': game_json['seasonId'],
            'team_1': team_1,
            'team_2': team_2
        }
        return game

    @staticmethod
    def get_teams_score(game_id, game_hash):
        for i in range(1, 4):
            try:
                game_url = BASE_GAME_URL.format(i, game_id, game_hash)
                r = requests.get(game_url)
                game_json = r.json()
                break
            except Exception:
                pass

        results = []
        game_id = game_json['gameId']
        for team in game_json['teams']:
            result = {
                'game_id': game_id,
                'baron_kills': team['baronKills'],
                'dragon_kills': team['dragonKills'],
                'first_baron': team['firstBaron'],
                'first_blood': team['firstBlood'],
                'first_dragon': team['firstDragon'],
                'first_inhibitor': team['firstInhibitor'],
                'first_rift_herald': team['firstRiftHerald'],
                'first_tower': team['firstTower'],
                'inhibitor_kills': team['inhibitorKills'],
                'rift_herald_kills': team['riftHeraldKills'],
                'team_id': team['teamId'],
                'tower_kills': team['towerKills'],
                'win': team['win']
            }
            results.append(result)

        return results

    @staticmethod
    def get_team_bans(game_id, game_hash):
        for i in range(1, 4):
            try:
                game_url = BASE_GAME_URL.format(i, game_id, game_hash)
                r = requests.get(game_url)
                game_json = r.json()
                break
            except Exception:
                pass

        bans = []
        game_id = game_json['gameId']
        for team in game_json['teams']:
            team_bans = []
            for ban in team['bans']:
                b = {
                    'team_id': team['teamId'],
                    'game_id': game_id,
                    'champion_id': ban['championId'],
                    'pick_turn': ban['pickTurn'],
                }
                team_bans.append(b)
            bans.append(team_bans)

        return bans

    @staticmethod
    def get_players_scores(game_id, game_hash):
        for i in range(1, 4):
            try:
                game_url = BASE_GAME_URL.format(i, game_id, game_hash)
                r = requests.get(game_url)
                game_json = r.json()
                break
            except Exception:
                pass

        players_stats = []
        game_id = game_json['gameId']

        for player in game_json['participants']:
            player_stats = {}
            player_stats['game_id'] = game_id
            player_stats['championId'] = player['championId']
            player_stats['participantId'] = player['participantId']
            player_stats['spell1Id'] = player['spell1Id']
            player_stats['spell2Id'] = player['spell2Id']
            player_stats['assists'] = player['stats']['assists']
            player_stats['champLevel'] = player['stats']['champLevel']
            player_stats['combatPlayerScore'] = player['stats']['combatPlayerScore']
            player_stats['damageDealtToObjectives'] = player['stats']['damageDealtToObjectives']
            player_stats['damageDealtToTurrets'] = player['stats']['damageDealtToTurrets']
            player_stats['damageSelfMitigated'] = player['stats']['damageSelfMitigated']
            player_stats['deaths'] = player['stats']['deaths']
            player_stats['doubleKills'] = player['stats']['doubleKills']
            player_stats['firstBloodAssist'] = player['stats']['firstBloodAssist']
            player_stats['firstBloodKill'] = player['stats']['firstBloodKill']
            player_stats['firstInhibitorAssist'] = player['stats']['firstInhibitorAssist']
            player_stats['firstInhibitorKill'] = player['stats']['firstInhibitorKill']
            player_stats['firstTowerAssist'] = player['stats']['firstTowerAssist']
            player_stats['firstTowerKill'] = player['stats']['firstTowerKill']
            player_stats['goldEarned'] = player['stats']['goldEarned']
            player_stats['goldSpent'] = player['stats']['goldSpent']
            player_stats['inhibitorKills'] = player['stats']['inhibitorKills']
            player_stats['item0'] = player['stats']['item0']
            player_stats['item1'] = player['stats']['item1']
            player_stats['item2'] = player['stats']['item2']
            player_stats['item3'] = player['stats']['item3']
            player_stats['item4'] = player['stats']['item4']
            player_stats['item5'] = player['stats']['item5']
            player_stats['item6'] = player['stats']['item6']
            player_stats['killingSprees'] = player['stats']['killingSprees']
            player_stats['kills'] = player['stats']['kills']
            player_stats['largestCriticalStrike'] = player['stats']['largestCriticalStrike']
            player_stats['largestKillingSpree'] = player['stats']['largestKillingSpree']
            player_stats['largestMultiKill'] = player['stats']['largestMultiKill']
            player_stats['longestTimeSpentLiving'] = player['stats']['longestTimeSpentLiving']
            player_stats['magicDamageDealt'] = player['stats']['magicDamageDealt']
            player_stats['magicDamageDealtToChampions'] = player['stats']['magicDamageDealtToChampions']
            player_stats['magicalDamageTaken'] = player['stats']['magicalDamageTaken']
            player_stats['neutralMinionsKilled'] = player['stats']['neutralMinionsKilled']
            player_stats['neutralMinionsKilledEnemyJungle'] = player['stats']['neutralMinionsKilledEnemyJungle']
            player_stats['neutralMinionsKilledTeamJungle'] = player['stats']['neutralMinionsKilledTeamJungle']
            player_stats['objectivePlayerScore'] = player['stats']['objectivePlayerScore']
            player_stats['participantId'] = player['stats']['participantId']
            player_stats['pentaKills'] = player['stats']['pentaKills']
            player_stats['perk0'] = player['stats']['perk0']
            player_stats['perk0Var1'] = player['stats']['perk0Var1']
            player_stats['perk0Var2'] = player['stats']['perk0Var2']
            player_stats['perk0Var3'] = player['stats']['perk0Var3']
            player_stats['perk1'] = player['stats']['perk1']
            player_stats['perk1Var1'] = player['stats']['perk1Var1']
            player_stats['perk1Var2'] = player['stats']['perk1Var2']
            player_stats['perk1Var3'] = player['stats']['perk1Var3']
            player_stats['perk2'] = player['stats']['perk2']
            player_stats['perk2Var1'] = player['stats']['perk2Var1']
            player_stats['perk2Var2'] = player['stats']['perk2Var2']
            player_stats['perk2Var3'] = player['stats']['perk2Var3']
            player_stats['perk3'] = player['stats']['perk1']
            player_stats['perk3Var1'] = player['stats']['perk3Var1']
            player_stats['perk3Var2'] = player['stats']['perk3Var2']
            player_stats['perk3Var3'] = player['stats']['perk3Var3']
            player_stats['perk4'] = player['stats']['perk4']
            player_stats['perk4Var1'] = player['stats']['perk4Var1']
            player_stats['perk4Var2'] = player['stats']['perk4Var2']
            player_stats['perk4Var3'] = player['stats']['perk4Var3']
            player_stats['perk5'] = player['stats']['perk5']
            player_stats['perk5Var1'] = player['stats']['perk5Var1']
            player_stats['perk5Var2'] = player['stats']['perk5Var2']
            player_stats['perk5Var3'] = player['stats']['perk5Var3']
            player_stats['perkPrimaryStyle'] = player['stats']['perkPrimaryStyle']
            player_stats['perkSubStyle'] = player['stats']['perkSubStyle']
            player_stats['physicalDamageDealt'] = player['stats']['physicalDamageDealt']
            player_stats['physicalDamageDealtToChampions'] = player['stats']['physicalDamageDealtToChampions']
            player_stats['physicalDamageTaken'] = player['stats']['physicalDamageTaken']
            player_stats['quadraKills'] = player['stats']['quadraKills']
            player_stats['sightWardsBoughtInGame'] = player['stats']['sightWardsBoughtInGame']
            player_stats['statPerk0'] = player['stats']['statPerk0']
            player_stats['statPerk1'] = player['stats']['statPerk1']
            player_stats['statPerk2'] = player['stats']['statPerk2']
            player_stats['timeCCingOthers'] = player['stats']['timeCCingOthers']
            player_stats['totalDamageDealt'] = player['stats']['totalDamageDealt']
            player_stats['totalDamageDealtToChampions'] = player['stats']['totalDamageDealtToChampions']
            player_stats['totalDamageTaken'] = player['stats']['totalDamageTaken']
            player_stats['totalHeal'] = player['stats']['totalHeal']
            player_stats['totalMinionsKilled'] = player['stats']['totalMinionsKilled']
            player_stats['totalPlayerScore'] = player['stats']['totalPlayerScore']
            player_stats['totalScoreRank'] = player['stats']['totalScoreRank']
            player_stats['totalTimeCrowdControlDealt'] = player['stats']['totalTimeCrowdControlDealt']
            player_stats['totalUnitsHealed'] = player['stats']['totalUnitsHealed']
            player_stats['tripleKills'] = player['stats']['tripleKills']
            player_stats['trueDamageDealt'] = player['stats']['trueDamageDealt']
            player_stats['trueDamageDealtToChampions'] = player['stats']['trueDamageDealtToChampions']
            player_stats['trueDamageTaken'] = player['stats']['trueDamageTaken']
            player_stats['turretKills'] = player['stats']['turretKills']
            player_stats['unrealKills'] = player['stats']['unrealKills']
            player_stats['visionScore'] = player['stats']['visionScore']
            player_stats['visionWardsBoughtInGame'] = player['stats']['visionWardsBoughtInGame']
            player_stats['wardsKilled'] = player['stats']['wardsKilled']
            player_stats['wardsPlaced'] = player['stats']['wardsPlaced']
            player_stats['win'] = player['stats']['win']
            player_stats['teamId'] = player['teamId']

            players_stats.append(player_stats)

        return players_stats


'''
championId: 6
highestAchievedSeasonTier: "UNRANKED"
participantId: 1
spell1Id: 4
spell2Id: 12
assists: 0
champLevel: 14
combatPlayerScore: 0
damageDealtToObjectives: 5567
damageDealtToTurrets: 0
damageSelfMitigated: 19892
deaths: 3
doubleKills: 0
firstBloodAssist: false
firstBloodKill: false
firstInhibitorAssist: false
firstInhibitorKill: false
firstTowerAssist: false
firstTowerKill: false
goldEarned: 8627
goldSpent: 8400
inhibitorKills: 0
item0: 3800
item1: 3047
item2: 2055
item3: 3071
item4: 1028
item5: 1033
item6: 3340
killingSprees: 0
kills: 1
largestCriticalStrike: 0
largestKillingSpree: 0
largestMultiKill: 1
longestTimeSpentLiving: 946
magicDamageDealt: 34
magicDamageDealtToChampions: 34
magicalDamageTaken: 5191
neutralMinionsKilled: 8
neutralMinionsKilledEnemyJungle: 0
neutralMinionsKilledTeamJungle: 3
objectivePlayerScore: 0
participantId: 1
pentaKills: 0
perk0: 8360
perk0Var1: 10
perk0Var2: 0
perk0Var3: 0
perk1: 8313
perk1Var1: 0
perk1Var2: 0
perk1Var3: 0
perk2: 8345
perk2Var1: 4
perk2Var2: 0
perk2Var3: 0
perk3: 8347
perk3Var1: 0
perk3Var2: 0
perk3Var3: 0
perk4: 8473
perk4Var1: 270
perk4Var2: 0
perk4Var3: 0
perk5: 8451
perk5Var1: 184
perk5Var2: 0
perk5Var3: 0
perkPrimaryStyle: 8300
perkSubStyle: 8400
physicalDamageDealt: 97501
physicalDamageDealtToChampions: 6762
physicalDamageTaken: 8238
playerScore0: 0
playerScore1: 0
playerScore2: 0
playerScore3: 0
playerScore4: 0
playerScore5: 0
playerScore6: 0
playerScore7: 0
playerScore8: 0
playerScore9: 0
quadraKills: 0
sightWardsBoughtInGame: 0
statPerk0: 5008
statPerk1: 5002
statPerk2: 5002
timeCCingOthers: 5
totalDamageDealt: 105672
totalDamageDealtToChampions: 7103
totalDamageTaken: 13817
totalHeal: 641
totalMinionsKilled: 194
totalPlayerScore: 0
totalScoreRank: 0
totalTimeCrowdControlDealt: 168
totalUnitsHealed: 1
tripleKills: 0
trueDamageDealt: 8136
trueDamageDealtToChampions: 306
trueDamageTaken: 388
turretKills: 0
unrealKills: 0
visionScore: 23
visionWardsBoughtInGame: 4
wardsKilled: 3
wardsPlaced: 11
win: false
teamId: 100
'''

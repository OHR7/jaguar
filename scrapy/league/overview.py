from scrapy import Scraper


class Overview(Scraper):
    def __init__(self, league, year, slug):
        super().__init__()
        self._prefix = '{}/{}/{}'.format(league, year, slug)
        self.page = self.get_raw_code(self._prefix)

    def get_overview(self):
        return

    def get_info(self):
        return

    def get_standings(self):
        return

import requests

from common import BASE_LEAGUE_URL


class Teams:
    def __init__(self):
        pass

    @staticmethod
    def get_team():
        r = requests.get(BASE_LEAGUE_URL)
        r_json = r.json()
        teams = []
        for game in r_json['schedules']:
            team_info = {
                'id': game['participant1']['id'],
                'acronym': game['participant1']['acronym'],
                'logo_url': game['participant1']['logoUrl'],
                'name': game['participant1']['name']
            }
            teams.append(team_info)

        return teams

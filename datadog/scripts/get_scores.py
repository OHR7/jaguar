from common import LEAGUE_SLUGS, YEARS, SEASONS, POSITIONS
from datadog.connection import Session
from scrapy.league.games import Games


def add_scores(session):
    # TODO Create array with league names
    for l in LEAGUE_SLUGS:
        for y in YEARS:
            for s in SEASONS:
                for w in range(1, 16):
                    # print(l + y + s)
                    if w == 1:
                        week = ''
                    else:
                        week = '/Week_{}'.format(w)
                    game_scraper = Games(l, y, s, week)
                    games = game_scraper.get_games_overview()
                    # if games:
                    #     for ts in games:
                    # print(ts['tournament'] + ts['team1'])
                    # pprint(games)
                    team_scores = game_scraper.get_team_scores()
                    if team_scores:
                        for ts in team_scores:
                            print(ts['team1ban1'])
                    # pprint(team_scores)
                    for p in POSITIONS:
                        player_scores = game_scraper.get_players_scores(p)
                        # pprint(player_scores)


def main():
    session = Session()
    add_scores(session)


if __name__ == '__main__':
    main()
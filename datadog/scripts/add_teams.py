from sqlalchemy import exc

from common import LEAGUE_SLUGS, YEARS, SEASONS
from datadog.connection import Session
from datadog.models.team.team import Team
from scrapy.league.teams import Teams


def add_all_teams(session):
    for l in LEAGUE_SLUGS:
        for y in YEARS:
            for s in SEASONS:
                team_scraper = Teams(l, y, s)
                teams = team_scraper.get_team_names()
                print(teams)
                if teams:
                    for t in teams:
                        try:
                            team = Team(t)
                            session.add(team)
                            session.commit()
                        except exc.IntegrityError:
                            session.rollback()
                            print('This item fails one of the unique/foreign key checks')
                        except Exception as e:
                            print(e)
                            raise
    session.close()


def main():
    session = Session()
    add_all_teams(session)


if __name__ == '__main__':
    main()
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


engine = create_engine('mysql+pymysql://omar:password@127.0.0.1:3306/jaguar')
# engine.table_names()

Session = sessionmaker(bind=engine)
Base = declarative_base()

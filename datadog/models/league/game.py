# coding=utf-8

from sqlalchemy import Column, String, Integer, Date, ForeignKey

from datadog.connection import Base


class Game(Base):
    __tablename__ = 'game'

    id = Column(Integer, primary_key=True)
    duration = Column(Integer)
    patch = Column(String)
    season = Column(Integer)
    team1 = Column(Integer)
    team2 = Column(Integer)

    def __init__(self, game):
        self.patch = game['patch']
        self.team1 = game['team1']
        self.team2 = game['team2']
        self.winner = game['winner']
        self.team1_score = game['team1_score']
        self.team2_score = game['team2_score']
        self.date = game['date']
        self.game_length = game['game_length']
        self.tournament = game['tournament']

        if game['winner'] == '1':
            self.loser = game['team2']
        else:
            self.loser = game['team1']

from pprint import pprint

from scrapy.league.games import Games


def main():
    pass
    g = Games()
    games_ids = g.get_games_ids()
    # pprint(games_ids)
    for gid in games_ids:
        game_overview = g.get_game_overview(gid['game_id'], gid['game_hash'], gid['team_1'], gid['team_2'])
        # pprint(game_overview)
        team_score = g.get_teams_score(gid['game_id'], gid['game_hash'])
        # pprint(team_score)
        team_bans = g.get_team_bans(gid['game_id'], gid['game_hash'])
        # pprint(team_bans)
        players_scores = g.get_players_scores(gid['game_id'], gid['game_hash'])
        # pprint(players_scores)
        break


if __name__ == '__main__':
    main()
